import UIKit

protocol Navigator {
    func setHomeScreenAsRootController()
    func setTutorialScreenAsRootController()
    func setWelcomeFlowAsRootController()
}

class NavigatorImpl: Navigator {
    private func getMainWindow() -> UIWindow? {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let mainWindow = appDelegate.window {
                return mainWindow
            }
            
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.makeKeyAndVisible()
            appDelegate.window = window
        }
        return nil
    }
    
    func setHomeScreenAsRootController() {
        getMainWindow()?.rootViewController = UINavigationController(rootViewController: MainAssembly.shared.homeViewController())
    }
    
    
    func setTutorialScreenAsRootController() {
        // In a real app: Implement this to add tutorial flow to the application
    }
    
    func setWelcomeFlowAsRootController() {
        // In a real app: Implement this to add onboarding flow to the application
    }
}
