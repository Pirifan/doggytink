//
//  AppFonts.swift
//  Tinker
//
//  Created by Aleksandar Nikolovski on 12/22/18.
//  Copyright © 2018 Pirifan. All rights reserved.
//

import Foundation

struct Fonts {
    struct Dosis {
        static let semiBold = "Dosis-Semibold"
        static let extraBold = "Dosis-ExtraBold"
    }
}
