import UIKit

/**
 Centralized place for initializing view controllers and adding dependencies through dependency injection
 */
class MainAssembly: NSObject {
    static let shared = MainAssembly()
    
    func getGlobalNavigator() -> Navigator {
        return NavigatorImpl()
    }
    
    // MARK: Home screen
    func homeViewController() -> HomeViewController {
        return HomeViewController()
    }
    
    // MARK: Doggy Details screen
    func doggyDetailsController(_ imageString: String) -> DoggyDetailsViewController {
        let doggyDetailsVC = DoggyDetailsViewController()
        doggyDetailsVC.updateImage(with: imageString)
        return doggyDetailsVC
    }
}
