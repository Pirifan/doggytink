//
//  BaseViewController.swift
//  DoggyTink
//
//  Created by Aleksandar Nikolovski on 1/12/19.
//  Copyright © 2019 Pirifan. All rights reserved.
//

import UIKit

/// I can use this BaseViewController to additionally specify app wide configurations such as themes, navigation bars etc.
class BaseViewController: UIViewController {
    
    //MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.backgroundColor
        setupView()
        setupConstraints()
        setupNavigationBar()
    }
    
    //MARK: View Setup
    func setupView() { /* Override in child controllers */ }
    func setupConstraints() { /* Override in child controllers */ }
    func setupNavigationBar() {
        navigationController?.navigationBar.backgroundColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = UIColor.buttonsTintColor
        addTitle()
    }
    
    //MARK: Methods
    private func addTitle() {
        let titleFont = UIFont(name: Fonts.Dosis.semiBold, size: 20)
        let navigationTitleLabel = UILabel()
        navigationTitleLabel.text = "Doggy Tink"
        navigationTitleLabel.textColor = self.navigationController?.navigationBar.tintColor
        navigationTitleLabel.font = titleFont
        navigationItem.titleView = navigationTitleLabel
        navigationItem.titleView?.autoresizesSubviews = false
        navigationItem.titleView?.translatesAutoresizingMaskIntoConstraints = false
    }
}

