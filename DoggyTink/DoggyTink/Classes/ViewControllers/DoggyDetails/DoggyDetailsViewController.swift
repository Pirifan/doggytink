//
//  DoggyDetailsViewController.swift
//  DoggyTink
//
//  Created by Aleksandar Nikolovski on 1/12/19.
//  Copyright © 2019 Pirifan. All rights reserved.
//

import UIKit

class DoggyDetailsViewController: BaseViewController {
    var imageView = UIImageView()
    
    // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        
    }
    
    // MARK: View Setup
    override func setupView() {
        self.view.backgroundColor = UIColor.backgroundColor
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(imageZoomChanged(_:)))
        imageView.addGestureRecognizer(pinchGesture)
        self.view.addSubview(imageView)
    }
    
    override func setupConstraints() {
        imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        let height = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        imageView.heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    func updateImage(with imageString: String) {
        imageView.loadImageFromUrl(urlString: imageString)
    }
    
    // MARK: IBAction
    @objc func imageZoomChanged(_ sender: UIPinchGestureRecognizer) {
        let pinchCenter = CGPoint(x: sender.location(in: view).x - imageView.bounds.midX,
                                  y: sender.location(in: view).y - imageView.bounds.midY)
        let transform = imageView.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y)
            .scaledBy(x: sender.scale, y: sender.scale)
            .translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
        imageView.transform = transform
        sender.scale = 1.0
        
        if sender.state == .ended {
            UIView.animate(withDuration: 0.3) {
                self.imageView.transform = CGAffineTransform.identity
            }
            
        }
    }
    
}
