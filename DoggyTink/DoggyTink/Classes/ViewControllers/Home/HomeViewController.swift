//
//  HomeViewController.swift
//  DoggyTink
//
//  Created by Aleksandar Nikolovski on 1/12/19.
//  Copyright © 2019 Pirifan. All rights reserved.
//

import UIKit


class HomeViewController: BaseViewController {
    let DoggyCellIdentifier = "DoggyCell"
    let DoggyDetailsSegue = "DoggyDetailsSegue"
    let kNumberOfDoggies = "50"
    
    var collectionView: UICollectionView!
    var doggies = [String]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        getDoggies()
    }
    
    // MARK: View Setup
    override func setupView() {
        self.view.backgroundColor = UIColor.backgroundColor
        
        let flowLayout = UICollectionViewFlowLayout()
        let space = 1.0 as CGFloat
        
        let width = UIDevice.current.orientation.isLandscape ? self.view.bounds.height : self.view.bounds.width
        let cellWidth = width/4.0 - 1
        let cellHeight = cellWidth
        flowLayout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        flowLayout.minimumInteritemSpacing = space
        flowLayout.minimumLineSpacing = space
        flowLayout.sectionInsetReference = .fromSafeArea
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(DoggyCell.self, forCellWithReuseIdentifier: DoggyCellIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.clear
        
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.buttonsTintColor
        refreshControl.addTarget(self, action: #selector(pulledToRefresh), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        
        self.view.addSubview(collectionView)
    }
    
    override func setupConstraints() {
        collectionView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    // MARK: IBActions
    @objc func pulledToRefresh() {
        getDoggies()
    }
    
    // MARK: Methods
    private func getDoggies() {
        // Count parameter can be parametrized, and not kept as a constant
        DoggyService.getDoggies(parameters: ["count" : kNumberOfDoggies], response: { [ weak self] (doggies) in
            self?.doggies = doggies
            self?.collectionView.refreshControl?.endRefreshing()
        }) { [weak self] (error) in
            self?.showErrorWhileExecutingRequestAlert(with: error, completion: { [weak self] in
                self?.collectionView.refreshControl?.endRefreshing()
            })
        }
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return doggies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DoggyCellIdentifier, for: indexPath) as? DoggyCell{
            cell.setup(doggies[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(MainAssembly.shared.doggyDetailsController(doggies[indexPath.row]), animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
    }
}
