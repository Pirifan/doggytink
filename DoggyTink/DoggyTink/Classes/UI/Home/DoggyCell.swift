//
//  DoggyCell.swift
//  DoggyTink
//
//  Created by Aleksandar Nikolovski on 1/12/19.
//  Copyright © 2019 Pirifan. All rights reserved.
//

import UIKit

class DoggyCell: UICollectionViewCell {
    var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        imageView = UIImageView(frame: contentView.bounds)
        contentView.addSubview(imageView)
    }
    
    func setup(_ urlString: String) {
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.imageView.image = Images.noDog
        self.imageView.loadImageFromUrl(urlString: urlString)
    }
}
