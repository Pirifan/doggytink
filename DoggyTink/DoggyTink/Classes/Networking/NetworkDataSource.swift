import Foundation

struct NetworkDataSource {
    static func get(url: URL, successCallback: @escaping (Data) -> (), errorCallback: @escaping ((String?) -> ()?)) {
        let defaultSession = URLSession(configuration: .default)
        var dataTask: URLSessionDataTask?
        var errorMessage = String.init()
        
        dataTask = defaultSession.dataTask(with: url) { data, response, error in
            defer { dataTask = nil }
            
            if let error = error {
                errorMessage += "DataTask error: " + error.localizedDescription + "\n"
                errorCallback(errorMessage)
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                    DispatchQueue.main.async {
                        successCallback(data)
                    }
                }
        }
        dataTask?.resume()
    }
}
