//
//  ApiRouter.swift
//  DoggyTink
//
//  Created by Aleksandar Nikolovski on 1/12/19.
//  Copyright © 2019 Pirifan. All rights reserved.
//

import Foundation


enum APIRouter {
    
    case getDoggies(parameters: [String:String])
    
    var path: String {
        switch self {
            case .getDoggies: return "/bomb"
        }
    }
    
    var parameters: [String: String]? {
        switch self {
            case .getDoggies(let parameters): return parameters
        }
    }
    
    var baseURLString: String {
        switch self {
            case .getDoggies: return "https://pugme.herokuapp.com"
        }
    }
    
    func asURL() -> URL {
        let route = self.baseURLString + self.path
        var urlComponents = URLComponents(string: route)
        urlComponents?.queryItems = [URLQueryItem]()
        parameters?.forEach { urlComponents?.queryItems?.append(URLQueryItem(name: $0.key, value: $0.value)) }
        return urlComponents!.url!
    }
}
