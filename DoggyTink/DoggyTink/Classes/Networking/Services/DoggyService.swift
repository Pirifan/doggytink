//
//  DoggyService.swift
//  DoggyTink
//
//  Created by Aleksandar Nikolovski on 1/12/19.
//  Copyright © 2019 Pirifan. All rights reserved.
//

import Foundation

class DoggyService {
    static func getDoggies(parameters: [String:String], response: @escaping ([String]) -> (), errorCallback: @escaping (String) -> ()) {
        NetworkDataSource.get(url: APIRouter.getDoggies(parameters: parameters).asURL(), successCallback: { (resultData) in
            guard let doggies = try? JSONDecoder().decode(DoggiesResponse.self, from: resultData) else { return }
            response(doggies.pugs)
        }) { (error) in
            errorCallback(error ?? "Something bad happened.")
        }
    }
}
