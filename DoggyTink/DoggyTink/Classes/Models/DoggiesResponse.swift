//
//  Dog.swift
//  DoggyTink
//
//  Created by Aleksandar Nikolovski on 1/12/19.
//  Copyright © 2019 Pirifan. All rights reserved.
//

import Foundation

struct DoggiesResponse: Codable {
    var pugs: [String]
}
