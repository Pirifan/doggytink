//
//  UIImageExt.swift
//  DoggyTink
//
//  Created by Aleksandar Nikolovski on 1/12/19.
//  Copyright © 2019 Pirifan. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView{
    func loadImageFromUrl(urlString: String?){
        guard let urlString = urlString, let url = URL(string: urlString) else { return }
        self.contentMode = .scaleAspectFill
        self.clipsToBounds = true
        NetworkDataSource.get(url: url, successCallback: { [weak self] (imageData) in
            self?.image = UIImage(data: imageData)
        }) { (error) -> ()? in
            print("Error when retrieiving image")
        }
    }
}
