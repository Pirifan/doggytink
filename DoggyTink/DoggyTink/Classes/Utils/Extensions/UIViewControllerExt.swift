import Foundation
import UIKit

extension UIViewController {
    func showErrorWhileExecutingRequestAlert(with errorMessage: String?, completion: @escaping () -> ()) {
        guard let errorMessage = errorMessage else { return }
        let alertVC = UIAlertController(title: "An error occured", message: errorMessage, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alertVC, animated: true, completion: completion)
    }
}
