import UIKit

extension UIColor {
    class var buttonsTintColor: UIColor {
        return UIColor(displayP3Red: 248/255, green: 150/255, blue: 115/255, alpha: 1)
    }
    
    class var backgroundColor: UIColor {
        return UIColor(displayP3Red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
    }
}

