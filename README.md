# Doggy Tink

A developer's way of proving your skills.

## Description

**1. Code driven UI**  
The Doggy Tink app was developed entirely in code, without using storyboards. I didn't use any third party framework either for the UI or the networking part. However, I have to agree with the author of the assignment, a place crawling with pugs cannot be a bad UI :P. (the app has pull to refresh feature, as I couldn't get enough of the doggies).

**2. Code strucure**  
I tried to structure the application as I would have done with a real world app. It has the MainAssembly class which handles view controller initialization and dependency injection in a centralized way. The Navigator class handles the setting of an initial view controller. There wasn't any need for a Presenter to organize the app even better, as in this situation, it would have been an overkill for such a simple task.  

**3. Networking**  
In the networking part I have tried to imitate a real life network layer, however, this solution is just a simple 
implementation and is missing a lot of stuff to become a a real world app -
it doesn't have a good error handling process, neither it has any other methods than 'get'. However, in my opinion,
this wasn't that important because in nearly every project and every company I have started to work with on a project, the network layer was reused from other already existing projects :)  

**4. Testing**  
This application does not have unit tests implemented, however, if this is critical I can implement it afterwards.
I did test the Doggy Tink on iPhone X and most of the other devices through the simulator. There should not be any UI mistakes on different size screens and different device orientations.

**5. Improvements**  
Pagination in the colleciton view, better error handling for network requests (better network layer altogether), Unit and UI tests, infinite scroll in the collection view etc.



